import React, { Component, Fragment } from 'react';
import { Route, NavLink, Switch } from 'react-router-dom';
import Header from './UI/Header/Header';
import Home from './Containers/Home/Home';


export default class App extends Component {
    render() {
        return (
            <Fragment>
                <Header/>
                <Switch>
                    <Route patch="/posts" component={Home}/>
                    <Route patch="/" component={Home}/>
                </Switch>
            </Fragment>
        )
    }
}