import React, { Component } from 'react';
import './Add.css';

class Add extends Component {
    render() {
        return (
            <form>
              <input type="text" placeholder="Title"/>
              <textarea name="body" cols="20" rows="5"></textarea>
              <button onClick={this.props.saved}>Save</button>
            </form>
        )
    }
};

export default Add;
