import React from 'react';
import { NavLink } from 'react-router-dom';
import './Header.css';
import Home from '../../Containers/Home/Home';
import Add from '../Add/Add';


const Header = () => {
    return (
        <div className="header">
            <NavLink to="/posts" component={Home}>Home</NavLink>
            <NavLink to="/posts/add" component={Add}>Add</NavLink>
            <NavLink>About</NavLink>
            <NavLink>Contacts</NavLink>
        </div>
    )
};

export default Header;