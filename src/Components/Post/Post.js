import React from 'react';
import { Link } from 'react-router-dom';

const Post = props => {
    return (
        <div className="post">
            <span>date</span>
            <h3>title</h3>
            <Link className="readMore" to={`/posts/${props.id}`} >Read more </Link>
        </div>
    )
};

export default Post;